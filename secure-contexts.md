# Secure Contexts

Browsers require HTTPS for many new Web Platform APIs. Browser makers hope this will lead to better security overall. Carrot and stick approach.

Is http://localhost a secure context?

How do I set up https://localhost easily? tls-keygen

How can I deploy to testing/staging/production with HTTPS? use http2.live

https://www.chromestatus.com/feature/6269417340010496
https://permission.site
https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts/features_restricted_to_secure_contexts
https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts
https://tools.ietf.org/html/draft-west-let-localhost-be-localhost-06
https://w3c.github.io/webappsec-secure-contexts/
https://twitter.com/intenttoship/status/960472061880696832
https://twitter.com/intenttoship/status/959447751506497538
https://twitter.com/intenttoship/status/954141673344458754
https://twitter.com/search?q=intenttoship%20contexts&src=typd